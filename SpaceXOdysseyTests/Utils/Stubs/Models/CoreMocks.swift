//
//  CoreMocks.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 19/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

@testable import SpaceXOdyssey

extension Core {
    static let validCore: JSONValue = [
        "core_serial":"B1049",
        "flight":1,
        "block":5,
        "reused":false,
        "land_success":true,
        "landing_intent":true,
        "landing_type":"ASDS",
        "landing_vehicle":"OCISLY"
    ]
}
