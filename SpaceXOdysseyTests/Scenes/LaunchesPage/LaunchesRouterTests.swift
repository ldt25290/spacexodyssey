//
//  LaunchesRouterTests.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 02/01/2019.
//  Copyright © 2019 mdev. All rights reserved.
//

@testable import SpaceXOdyssey
import Quick
import Nimble

class LaunchesRouterTests: QuickSpec {
    
    override func spec() {
        describe("LaunchesRouter tests") {
            
            // MARK: Subject under test
            
            var sut: LaunchesRouter!
            var window: UIWindow!
            var launchesVC = LaunchesViewController()
            var launchesData: Launches = [try! Launch.validLaunch.decode()]
            
            // MARK: Test setup
            
            func setupAccountRouter() {
                sut = LaunchesRouter()
            }
            
            beforeEach {
                LaunchesNetworkInjector.networkManager = LaunchesNetworkManagerMock()
                super.setUp()
                window = UIWindow(frame: UIScreen.main.bounds)
                setupAccountRouter()
            }
            
            func loadview() {
                let nav = UINavigationController(rootViewController: launchesVC)
                window?.rootViewController = nav
                window?.makeKeyAndVisible()
            }
            
            afterEach {
                window = nil
                LaunchesNetworkInjector.networkManager = LaunchesNetworkManager()
            }
            
            // MARK: Tests
            
            context("Routing to show single launch") {
                it("Should display launch page") {
                    loadview()
                    let nav = window.rootViewController as? UINavigationController
                    let accountVC = nav?.topViewController as? LaunchesViewController
                    
                    expect(accountVC?.router?.dataStore?.launches).toEventuallyNot(beNil())
                    
                    accountVC?.router?.showLaunchPage(for: 91)
                    expect(accountVC?.navigationController?.viewControllers.count).toEventually(equal(2))
                    expect(accountVC?.navigationController?.topViewController).toEventually(beAKindOf(SingleLaunchPageViewController.self))
                }
            }
        }
    }
}
