//
//  MainTabBarTests.swift
//  SpaceXOdysseyTests
//
//  Created by Martin Lukacs on 17/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Quick
import Nimble
@testable import SpaceXOdyssey

class MainTabBarViewControllerTests: QuickSpec {
    
    override func spec() {
        
        describe("MainTabBarViewController tests") {
            
            // MARK: Subject under test
            
            var sut: MainTabBarViewController!
            var window: UIWindow!
            
            // MARK: Test setup
            
            func setupMainTabBarViewController() {
                sut = MainTabBarViewController()
            }
            
            beforeSuite {
                super.setUp()
                window = UIWindow(frame: UIScreen.main.bounds)
                setupMainTabBarViewController()
            }
            
            afterSuite {
                window = nil
            }
            
            //MARK: - Test
            it("Should be a MainTabBarViewController") {
                expect(sut).to(beAKindOf(MainTabBarViewController.self))
            }
            
            it("Should contain 4 tabBBar items") {
                expect(sut.viewControllers?.count) == 4
            }
            
            it("Should have lauchesViewController as first and selected item") {
                guard let destinationVC = sut.viewControllers?[0] as? UINavigationController else {
                    fail("Should be a navigation Controller")
                    return
                }
                expect(destinationVC.topViewController!).to(beAKindOf(LaunchesViewController.self))
                expect(sut.tabBar.selectedItem?.title) == TabBarControllers.launches.getName()
            }
        }
    }
}
