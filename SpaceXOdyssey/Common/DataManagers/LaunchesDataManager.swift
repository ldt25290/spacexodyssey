//
//  LaunchesDataManager.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import PromiseKit

/// Name of the protocol to inject the network dependency managing the launches
protocol LaunchesNetworkInjected { }

/// Structure used to inject a instance of LaunchesDataManager into the LaunchesNetworkInjected protocol
struct LaunchesNetworkInjector {
    static var networkManager: LaunchesDataManager = LaunchesNetworkManager()
}

// MARK: - Extension of protocol including variable containing mechanism to inject
extension LaunchesNetworkInjected {
    var launchesDataManager: LaunchesDataManager {
        return LaunchesNetworkInjector.networkManager
    }
}

protocol LaunchesDataManager: class {
    func getLaunches(limit: Int?, offset: Int?, _ debugMode: Bool) -> Promise<Launches>
    func getLaunch(flightNumber: Int, _ debugMode: Bool) -> Promise<Launch>
}

extension LaunchesDataManager {
    func getLaunches(limit: Int? = nil, offset: Int? = nil, _ debugMode: Bool = false) -> Promise<Launches> {
        return getLaunches(limit: limit, offset: offset, debugMode)
    }
}

/// Class implementing the LaunchesDataManager protocol. Used by LaunchesNetworkInjector in non test cases
final class LaunchesNetworkManager: LaunchesDataManager {
    /// Gets all the SpaceX launches
    ///
    /// - Parameters:
    ///   - limit: Max Number of element by call
    ///   - offset: offset to apply to the call for pagination purposes
    ///   - debugMode: Togles Moya's verbose mode in console
    /// - Returns: Promise containing the launches
    func getLaunches(limit: Int?, offset: Int?, _ debugMode: Bool) -> Promise<Launches> {
        return APIManager.callApi(LaunchAPI.getAllLaunches(limit: nil, offset: nil), dataReturnType: Launches.self, debugMode: debugMode)
    }
    
    /// Get one specific launch
    ///
    /// - Parameters:
    ///   - flightNumber: The flight number of the desired launch
    ///   - debugMode: Togles Moya's verbose mode in console
    /// - Returns: Promise containing a specific launch
    func getLaunch(flightNumber: Int, _ debugMode: Bool) -> Promise<Launch> {
        return APIManager.callApi(LaunchAPI.getLaunch(flightNumber: flightNumber), dataReturnType: Launch.self, debugMode: debugMode)
    }
}
