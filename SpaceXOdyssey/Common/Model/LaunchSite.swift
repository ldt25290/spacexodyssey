//
//  LaunchSite.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

struct LaunchSite: Codable {
    let siteID: SiteID
    let siteName: SiteName
    let siteNameLong: SiteNameLong
    
    enum CodingKeys: String, CodingKey {
        case siteID = "site_id"
        case siteName = "site_name"
        case siteNameLong = "site_name_long"
    }
}

enum SiteID: String, Codable {
    case ccafsSlc40 = "ccafs_slc_40"
    case kscLc39A = "ksc_lc_39a"
    case kwajaleinAtoll = "kwajalein_atoll"
    case vafbSlc4E = "vafb_slc_4e"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try SiteID(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum SiteName: String, Codable {
    case ccafsSlc40 = "CCAFS SLC 40"
    case kscLc39A = "KSC LC 39A"
    case kwajaleinAtoll = "Kwajalein Atoll"
    case vafbSlc4E = "VAFB SLC 4E"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try SiteName(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum SiteNameLong: String, Codable {
    case capeCanaveralAirForceStationSpaceLaunchComplex40 = "Cape Canaveral Air Force Station Space Launch Complex 40"
    case kennedySpaceCenterHistoricLaunchComplex39A = "Kennedy Space Center Historic Launch Complex 39A"
    case kwajaleinAtollOmelekIsland = "Kwajalein Atoll Omelek Island"
    case vandenbergAirForceBaseSpaceLaunchComplex4E = "Vandenberg Air Force Base Space Launch Complex 4E"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try SiteNameLong(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}
