//
//  Core.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

struct Core: Codable {
    let coreSerial: String?
    let flight, block: Int?
    let gridfins, legs, reused, landSuccess: Bool?
    let landingIntent: Bool?
    let landingType: LandingType?
    let landingVehicle: LandingVehicle?
    
    enum CodingKeys: String, CodingKey {
        case coreSerial = "core_serial"
        case flight, block, gridfins, legs, reused
        case landSuccess = "land_success"
        case landingIntent = "landing_intent"
        case landingType = "landing_type"
        case landingVehicle = "landing_vehicle"
    }
}

enum LandingType: String, Codable {
    case asds = "ASDS"
    case ocean = "Ocean"
    case rtls = "RTLS"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try LandingType(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}

enum LandingVehicle: String, Codable {
    case jrti = "JRTI"
    case jrti1 = "JRTI-1"
    case lz1 = "LZ-1"
    case lz2 = "LZ-2"
    case lz4 = "LZ-4"
    case ocisly = "OCISLY"
    case unknown
    
    public init(from decoder: Decoder) throws {
        self = try LandingVehicle(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
    }
}
