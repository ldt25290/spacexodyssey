//
//  Optional+Extensions.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 19/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

extension Optional {
    func or(_ other: Optional) -> Optional {
        switch self {
        case .none: return other
        case .some: return self
        }
    }
}

extension Optional {
    func resolve(with error: @autoclosure () -> Error) throws -> Wrapped {
        switch self {
        case .none: throw error()
        case .some(let wrapped): return wrapped
        }
    }
}

