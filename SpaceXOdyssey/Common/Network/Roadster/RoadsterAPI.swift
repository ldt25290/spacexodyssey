//
//  RoadsterAPI.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 20/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Moya

enum RoadsterAPI {
    case getRoadster()
}

extension RoadsterAPI: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var path: String {
        switch self {
        case .getRoadster:
            return "/roadster/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getRoadster:
            return .get
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getRoadster:
            return URLEncoding.methodDependent
        }
    }
    
    var task: Task {
        switch self {
        case .getRoadster:
            return .requestPlain
        }
    }
}
