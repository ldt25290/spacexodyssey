//
//  RoadsterAPIMockData.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 20/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Foundation

extension RoadsterAPI {
    var sampleData: Data {
        switch self {
        case .getRoadster:
            return stubbedResponse("Roadster")
        }
    }
}

