//
//  LauncheAPI.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 18/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import Moya

enum LaunchAPI {
    case getAllLaunches(limit: Int?, offset: Int?)
    case getLaunch(flightNumber: Int)
}

extension LaunchAPI: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var path: String {
        switch self {
        case .getAllLaunches:
            return "/launches/"
        case .getLaunch(let flightNumber):
            return "/launches/\(flightNumber)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllLaunches, .getLaunch:
            return .get
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getAllLaunches, .getLaunch:
            return URLEncoding.methodDependent
        }
    }
    
    var task: Task {
        switch self {
        case .getAllLaunches(let limit, let offset):
            var params: [String: Any] = [:]
            params["limit"] = limit
            params["offset"] = offset
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getLaunch:
            return .requestPlain
        }
    }
}

