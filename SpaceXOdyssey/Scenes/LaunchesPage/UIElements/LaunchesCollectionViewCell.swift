//
//  LaunchesCollectionViewCell.swift
//  SpaceXOdyssey
//
//  Created by Martin Lukacs on 23/12/2018.
//  Copyright © 2018 mdev. All rights reserved.
//

import UIKit
import SkeletonView

class LaunchesCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var imgMain: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSkeletonable = true
    }
    
    func update(item: LaunchesPage.DisplayedLaunch) {
        lblTitle.text = item.name ?? ""
        imgMain.download(image: item.imgUrl ?? "")
    }
    
    func getSize(_ width: CGFloat) -> CGSize {
        widthConstraint.constant = width
        guard let title = lblTitle.text else {
             return CGSize(width: width, height: width)
        }
        let titleWidth = width - 10
        let totalHeight = width + title.height(constraintedWidth: titleWidth, font: lblTitle.font) + 10
        return CGSize(width: width, height: totalHeight)
    }
}
