# Space X Odyssey


This project is part of a serie of articles explaining how to implement the clean swift architecture.

This is a simple application based on Space X API. It displays all the launches done by the company. 

It contains a search, a pull to refresh and a remote data fetching functionality.

To read the full series of article please follow this link 

* [Space X & clean swift series]()

Hope you'll enjoy the lecture.

Don't hesitate to give some feedback.
